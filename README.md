# Desafio frontend Conexa

Este projeto é um desafio proposto pela equipe da Conexa. A proposta deste desafio é a seguinte:

> Precisamos construir um front onde nossos médicos de plantão consigam ver as consultas agendadas e agendar novas consultas.***

> Será necessário ter uma tela de login para que médicos da clínica consigam acessar a aplicação, utilizando email e senha (credenciais para teste).

> A response do login será um token de validação e o nome do médico.

> O médico poderá listar e cadastrar consultas.

#### As tecnologias que utilizei nesse desafio foram:

- ReactJS
- Axios
- Typecript
- Jest
- React Router Dom
- CSS Modules

#### Como testar?

Para rodar o projeto é necessário ter instalado o Node.js em usa máquina [clique aqui para baixar caso não tenha](https://nodejs.org/en/)

Vamos começar iniciando o backend de testes, este backend foi criado pela equipe da Conexa: 

- Usando o terminal, acesse a pasta `backend` deste projeto e instale as dependencias utilizando o comando 

> npm install

- após instalar as dependencias rode o comando abaixo para iniciar o servidor.

> npm run start

Para iniciar o frontend o processo é o mesmo: 
- acesse a pasta do frontend:
> cd frontend
- instale as dependencias
> npm install
-  inicie o projeto
> npm run start

O projeto vai rodar na porta 3000, que pode ser acessádo pelo link [http://localhost:3000](http://localhost:3000)

### Testes unitarios
Para rodar os testes unitários basta rodar o comando abaixo:

> npm run test

### Build e deploy

Use o comando abaixo para gerar o codigo de build e transfira o código gerado para seu servidor de hospedagem.

# Estrutura do projeto

A estrutura deste projeto segue um padrão que se assemelha ao DDD (domain driven design), caso precise endender melhor do que se trata [clique aqui para ler um artigo sobre](https://blog.codecasts.com.br/arquitetura-de-projetos-vue-js-com-ddd-a2bc26817793)

Dentro da pasta ***src*** a distribuição é a seguinte:

```
|- app                # Modulos do projeto
  |- auth             # Componentes e páginas relacionadas a autenticação
  |-consultation      # Componentes e páginas relacionadas as consultas agendadas
  |- not-found        # Página de erro 404
   -- Routes.tsx      # Componente roteador
|- assets             # Imagens e icones 
|- domain             # Métodos de acesso a API
|- services           # Serviços
|- tests              # Testes unitários
|- support            # Componentes compartilhados

```
