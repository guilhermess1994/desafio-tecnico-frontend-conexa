import styles from "./Header.module.css";
import conexaLogo from "assets/conexa-logo.svg";
import Button from "support/components/Button";
import { clearStorage, getUsername, isAuthenticated } from "services/StorageService";
import { useHistory } from "react-router";
import React from 'react';

const Header: React.FC = () => {
  
  const history = useHistory();
  
  const handleLogout = () => {
    clearStorage();
    history.push('/');
  };

  return (
    <div
      className={`${styles.header} ${!isAuthenticated() && styles.centerContent}`}
    >
      <img src={conexaLogo} alt="Conexa" className={styles.headerLogo} />
      {isAuthenticated() && (
        <div>
          <span className={styles.username}>Olá, Dr. {getUsername()}</span>
          <Button outlined onClick={handleLogout}>
            Sair
          </Button>
        </div>
      )}
    </div>
  );
};

export default Header;
