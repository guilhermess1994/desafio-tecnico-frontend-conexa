import { FormEventHandler, ReactNode } from "react";

type Props = {
  children: ReactNode;
  onSubmit: FormEventHandler;
  className?: string;
};

const Form = ({ className, children, onSubmit }: Props) => {
  return <form className={className} onSubmit={onSubmit}>{children}</form>;
};

export default Form;
