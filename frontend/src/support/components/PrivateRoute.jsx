import { Route, Redirect } from 'react-router-dom';
import { isAuthenticated } from 'services/StorageService';

const PrivateRoute = ({component: Component, ...rest}) => {
  return (
    <Route {...rest} render={ props => (
      isAuthenticated() ? <Component {...props} /> : <Redirect to="/auth"/>
    )}/>
  );
}

export default PrivateRoute;