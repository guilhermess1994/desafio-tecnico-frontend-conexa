import { MouseEventHandler, ReactNode } from "react";
import styles from "./Modal.module.css";
import Button from "support/components/Button";
type Props = {
  show: boolean;
  children: ReactNode;
  onClose: MouseEventHandler;
};

const Modal = ({ children, show, onClose }: Props) => {
  return (
    <div className={`${styles.modalWrapper} ${!show ? styles.hidden : ""}`}>
      <div className={styles.modal}>
        <div className={styles.header}>
          <Button outlined onClick={onClose}>X</Button>
        </div>
        {children}
      </div>
    </div>
  );
};

export default Modal;
