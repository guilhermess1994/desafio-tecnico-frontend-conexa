import { useState } from "react";
import styles from "./DatePicker.module.css";

type Props = {
  label: string;
  placeholder: string;
  name: string;
  required?: boolean;
  value?: string;
  onChange: any;
};

const DatePicker = ({ name, value, placeholder, onChange, label, required }: Props) => {
  const [focus, setFocus] = useState(false);

  const handleFocus = () => setFocus(true);
  const handleBlur = () => setFocus(false);

  return (
    <div
      className={`${styles.datepickerWrapper} ${
        focus ? styles.datepickerWrapperFocus : ""
      }`}
    >
      <label className={styles.label} htmlFor={name}>
        {label}
      </label>

      <input
        onFocus={handleFocus}
        onBlur={handleBlur}
        onChange={onChange}
        type="date"
        name={name}
        value={value}
        required={required}
        placeholder={placeholder}
        className={styles.datepicker}
      />

    </div>
  );
};

export default DatePicker;
