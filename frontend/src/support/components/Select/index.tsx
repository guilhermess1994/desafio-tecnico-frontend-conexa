import { useState } from "react";
import styles from "./Select.module.css";

type Patient = {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
};

type Props = {
  items: Patient[];
  label: string;
  placeholder: string;
  name: string;
  required?: boolean;
  value?: string;
  onChange: any;
};

const Select = ({
  items,
  name,
  value,
  placeholder,
  onChange,
  label,
  required,
}: Props) => {
  const [focus, setFocus] = useState(false);

  const handleFocus = () => setFocus(true);
  const handleBlur = () => setFocus(false);

  const options = items.map((item) => (
    <option key={item.id} value={item.id}>
      {item.first_name} {item.last_name}
    </option>
  ));

  return (
    <div
      className={`${styles.selectWrapper} ${
        focus ? styles.selectWrapperFocus : ""
      }`}
    >
      <label className={styles.label} htmlFor={name}>
        {label}
      </label>

      <select
        onFocus={handleFocus}
        onBlur={handleBlur}
        onChange={onChange}
        value={value}
        name={name}
        required={required}
        placeholder={placeholder}
        className={styles.select}
      >
        {options}
      </select>
    </div>
  );
};

export default Select;
