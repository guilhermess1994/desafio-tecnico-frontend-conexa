import { MouseEventHandler, ReactNode } from "react";
import styles from "./Button.module.css";
import { VscLoading } from "react-icons/vsc";

type Props = {
  children: ReactNode;
  onClick?: MouseEventHandler;
  type?: "button" | "submit";
  outlined?: boolean;
  block?: boolean;
  isLoading?: boolean;
};

const Button = ({ type, block, isLoading, children, outlined, onClick }: Props) => {
  return (
    <button
      type={type}
      className={`${styles.button} ${outlined ? styles.outlined : ''} ${block ? styles.block : ''}`}
      onClick={onClick}
    >
      {isLoading ? <VscLoading className={styles.loadIcon}/> : children }
    </button>
  );
};

export default Button;
