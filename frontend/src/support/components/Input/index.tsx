import { useState } from "react";
import styles from "./Input.module.css";

type Props = {
  type: "text" | "password" | "email" | "number" | "date" | "time" | "datetime" | "datetime-local";
  label: string;
  placeholder: string;
  name: string;
  required?: boolean;
  value?: string;
  onChange?: any;
};

const Input = ({ type, name, value, onChange, placeholder, label, required }: Props) => {
  const [focus, setFocus] = useState(false);

  const handleFocus = () => setFocus(true);
  const handleBlur = () => setFocus(false);

  return (
    <div
      className={`${styles.inputWrapper} ${
        focus ? styles.inputWrapperFocus : ""
      }`}
    >
      <label className={styles.label} htmlFor={name}>
        {label}
      </label>

      <input
        onFocus={handleFocus}
        onBlur={handleBlur}
        onChange={onChange}
        type={type}
        name={name}
        required={required}
        placeholder={placeholder}
        className={styles.input}
      />

    </div>
  );
};

export default Input;
