export const isAuthenticated = () => localStorage.getItem("AUTH_TOKEN") !== null;

export const clearStorage = () => localStorage.clear();

export const getToken = () => localStorage.getItem("AUTH_TOKEN");

export const setToken = (payload: string) => localStorage.setItem("AUTH_TOKEN", payload);

export const getUsername = () => localStorage.getItem("AUTH_USERNAME");

export const setUsername = (payload: string) => localStorage.setItem("AUTH_USERNAME", payload);