import axios from 'axios';
import { getToken } from 'services/StorageService';

const httpService = axios.create({
  baseURL: 'http://localhost:3333'
});

httpService.interceptors.request.use(/* istanbul ignore next */ (config) => {
  const token = getToken();
  if (token) {
    config.headers.Authorization = token;
  }
  return config;
});


export default httpService;

