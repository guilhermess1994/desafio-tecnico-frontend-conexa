import http from 'services/HttpService';

class PatientsRequest {

  static list() {
    return http.get('/patients');
  }

}
export default PatientsRequest;