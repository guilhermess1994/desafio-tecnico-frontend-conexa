import http from 'services/HttpService';

type CreatePayload = {

}
class ConsultationRequest {

  static list() {
    return http.get('/consultations?_expand=patient');
  }

  static create(payload: CreatePayload) {
    return http.post('/consultations', payload);
  }
}
export default ConsultationRequest;