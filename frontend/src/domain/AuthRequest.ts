import http from 'services/HttpService';

type Login = {
  email: string;
  password: string;
}

class AuthRequest {

  static login(payload: Login) {
    return http.post('/login', payload);
  }
}
export default AuthRequest;