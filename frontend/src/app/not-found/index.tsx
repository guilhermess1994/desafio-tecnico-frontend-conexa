import React from "react";
import styles from "./styles/NotFound.module.css";
import Button from "support/components/Button";
import { useHistory } from "react-router";

const NotFound: React.FC = () => {
  const history = useHistory();

  const goToHome = () => {
    history.push("/");
  };
  return (
    <div className={styles.notfoundWrapper}>
      <div className={styles.title}>Oops</div>
      <div className={styles.subtitle}>Página não encontrada</div>
      <Button onClick={goToHome}>Voltar para página Inicial</Button>
    </div>
  );
};

export default NotFound;
