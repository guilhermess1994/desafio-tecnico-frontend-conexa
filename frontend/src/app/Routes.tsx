import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import PrivateRoute from 'support/components/PrivateRoute';
import NotFound from 'app/not-found';
import Auth from 'app/auth';
import Consultation from 'app/consultation';

const app: React.FC = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/auth" component={Auth} exact />
        <PrivateRoute path="/" component={Consultation} exact />
        <Route path="*" component={NotFound}/>
      </Switch>
    </BrowserRouter>
  );
};
export default app;
