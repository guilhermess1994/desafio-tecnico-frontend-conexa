import React from "react";
import Login from "./pages/Login";
import Header from "support/layout/Header";
import Container from "support/layout/Container";

const Auth: React.FC = () => {
  return (
    <div className="login">
      <Header />
      <Container>
        <Login />
      </Container>
    </div>
  );
};

export default Auth;
