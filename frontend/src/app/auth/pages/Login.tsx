import React, { useEffect } from 'react';
import styles from '../styles/Login.module.css';
import LoginForm from "../components/LoginForm";
import LoginPresentation from "../components/LoginPresentation";
import { clearStorage } from "services/StorageService";

const Login: React.FC = () => {

  useEffect( () => {
    clearStorage();
  },[]);
  
  return (
  <div className={styles.login}>
    <LoginPresentation/>
    <LoginForm/>
  </div>
  );
}

export default Login;