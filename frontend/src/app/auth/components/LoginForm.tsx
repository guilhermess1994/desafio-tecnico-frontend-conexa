import React, { useState } from "react";
import styles from "../styles/Login.module.css";
import ErrorMessage from "./ErrorMessage";
import Form from "support/components/Form";
import Button from "support/components/Button";
import Input from "support/components/Input";
import AuthRequest from "domain/AuthRequest";
import { useHistory } from "react-router";
import { setToken, setUsername } from "services/StorageService";

const LoginForm: React.FC = () => {

  const history = useHistory();
  const [hasError, setHasError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmit = (event: any) => {
    event.preventDefault();
    setHasError(false);
    setIsLoading(true);
    const payload = {
      email: event.target.email.value,
      password: event.target.password.value,
    };
    AuthRequest.login(payload)
      .then(({data}) => {
        setToken(data.token);
        setUsername(data.name);
        history.push('/');
      })
      .catch(() => setHasError(true))
      .finally(()=> setIsLoading(false));
  };

  return (
    <div className={styles.block}>
      <div className={styles.loginform}>
        <div className={styles.title}>Faça Login</div>

        <Form onSubmit={handleSubmit} className={styles.form}>
          <Input label="E-mail" name="email" placeholder="Digite seu e-mail" type="email" required />
          <Input label="Senha" name="password" placeholder="Digite sua senha" type="password" required />
          { hasError && <ErrorMessage /> }
          <Button type="submit" block isLoading={isLoading}>Entrar</Button>
        </Form>
      </div>
    </div>
  );
};

export default LoginForm;
