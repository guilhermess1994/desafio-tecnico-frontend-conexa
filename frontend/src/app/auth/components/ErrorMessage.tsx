import React from 'react';
import styles from "../styles/Login.module.css";
const components: React.FC = () => {
  return <div className={styles.errorMessage}>
    Credenciais inválidas
  </div>;
}

export default components;