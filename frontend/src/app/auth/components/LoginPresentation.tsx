import React from 'react';
import LoginIllustration from "assets/login-illustration.svg";
import styles from '../styles/Login.module.css';

const LoginPresentation: React.FC = () => {
  return (
    <div className={`${styles.block} ${styles.presentation}`}>
      <img className={styles.presentationImage} src={LoginIllustration} alt="Login Illustration"/>
    </div>
  );
}

export default LoginPresentation;