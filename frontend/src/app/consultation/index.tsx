import React from "react";
import Header from "support/layout/Header";
import Container from "support/layout/Container";
import ConsultationList from "./pages/ConsultationList";

const Consultation: React.FC = () => {
  return (
    <div className="login">
      <Header />
      <Container>
        <ConsultationList />
      </Container>
    </div>
  );
};

export default Consultation;
