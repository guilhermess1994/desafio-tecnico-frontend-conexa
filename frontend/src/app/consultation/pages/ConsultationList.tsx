import React, { useEffect, useState } from "react";
import styles from "../styles/ConsultationList.module.css";
import ConsultationFooter from "../components/ConsultationFooter";
import List from "../components/List";
import Modal from "support/components/Modal";
import ConsultationHelp from "../components/ConsultationHelp";
import EmptyList from "../components/EmptyList";
import ConsultationForm from "../components/ConsultationForm";
import ConsultationRequest from "domain/ConsultationRequest";

const ConsultationList: React.FC = () => {
  const [showModalCreate, setShowModalCreate] = useState(false);
  const [showModalHelp, setShowModalHelp] = useState(false);
  const [list, setList] = useState([]);

  const handleCreate = () => {
    setShowModalCreate(!showModalCreate);
  };

  const handleHelp = () => {
    setShowModalHelp(!showModalHelp);
  };

  const handleFormCreate = () => {
    setShowModalCreate(false);
    fetchConsultations();
  };

  const fetchConsultations = () => {
    ConsultationRequest.list().then(({ data }) => {
      setList(data);
    });
  };

  useEffect(() => {
    fetchConsultations();
  }, []);

  return (
    <>
      <div className={styles.consultationPage}>
        <h2 className={styles.title}>Consultas</h2>
        <div className={styles.content}>
          <div className={styles.consultationListWrapper}>
            {list.length ? <List items={list} /> : <EmptyList />}
          </div>
        </div>
        <ConsultationFooter
          handleCreate={handleCreate}
          handleHelp={handleHelp}
        />
        <Modal show={showModalCreate} onClose={handleCreate}>
          <ConsultationForm onCreate={handleFormCreate}/>
        </Modal>
        <Modal show={showModalHelp} onClose={handleHelp}>
          <ConsultationHelp />
        </Modal>
      </div>
    </>
  );
};

export default ConsultationList;
