import styles from "../styles/ConsultationForm.module.css";
import Button from "support/components/Button";
import Select from "support/components/Select";
import Input from "support/components/Input";
import { FormEvent, useEffect, useState } from "react";
import PatientsRequest from "domain/PatientsRequest";
import ConsultationRequest from "domain/ConsultationRequest";
type Props = {
  onCreate: any;
};

const ConsultationForm = ({ onCreate }: Props) => {
  let [patients, setPatients] = useState([]);
  let [patient, setPatient] = useState("1");
  let [date, setDate] = useState("");
  let [hour, setHour] = useState("");

  const handlePatient = (event: any) => {
    setPatient(event.target.value);
  };
  const handleDate = (event: any) => {
    setDate(event.target.value);
  };
  const handleHour = (event: any) => {
    setHour(event.target.value);
  };
  const submitHandler = (event: FormEvent) => {
    event.preventDefault();
    const parsedDate = new Date(`${date} ${hour}`);
    ConsultationRequest.create({
      patientId: parseInt(patient),
      date: parsedDate.toString(),
    }).then(({ data }) => onCreate());

    setPatient("1");
    setDate("");
    setHour("");
  };
  const fetchPatients = () => {
    PatientsRequest.list().then(({ data }) => setPatients(data));
  };

  useEffect(() => {
    fetchPatients();
  }, []);

  return (
    <div className={styles.formWrapper}>
      <form onSubmit={submitHandler}>
        <Select
          items={patients}
          required
          onChange={handlePatient}
          placeholder="Selecione o paciente"
          name="paciente"
          label="Paciente"
        />

        <div className={styles.row}>
          <Input
            placeholder="Selecione a data"
            onChange={handleDate}
            required
            type="date"
            label="Data"
            name="date"
          />
          <Input
            placeholder="Selecione a hora"
            onChange={handleHour}
            required
            type="time"
            label="Hora"
            name="time"
          />
        </div>

        <Button type="submit" block>
          Salvar
        </Button>
      </form>
    </div>
  );
};

export default ConsultationForm;
