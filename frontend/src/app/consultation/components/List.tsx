import ListItem from "./ListItem";
import styles from "../styles/ConsultationList.module.css";

type Items = {
  id: number;
  pacienteId: number;
  patient: {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
  };
  date: string;
};
type Props = {
  items: Items[];
};

const List = ({ items }: Props) => {
  const list = items.map((item) => (
    <ListItem
      key={item.id}
      pacienteId={item.pacienteId}
      name={`${item.patient.first_name} ${item.patient.last_name}`}
      date={item.date}
    />
  ));
  return (
    <>
      <div className={styles.listTotalInfo}>
        {items.length} consulta{items.length !== 1 ? "s" : ""} agendada
        {items.length !== 1 ? "s" : ""}
      </div>
      {list}
    </>
  );
};

export default List;
