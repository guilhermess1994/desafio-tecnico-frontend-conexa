import styles from "../styles/ConsultationList.module.css";
import Button from "support/components/Button";
import dayjs from "dayjs";

type Props = {
  key: number;
  pacienteId?: number;
  name: string;
  date: string;
};

const ListItem = ({ date, name }: Props) => {

  function formatDate(date: string) {
    return `${dayjs(date).format('DD/MM/YYYY')} às ${dayjs(date).format('hh:mm')}`
  }

  return (
    <div className={styles.listItem}>
      <div>
        <div className={styles.listItemContentUser}>{name}</div>
        <div className={styles.listItemContentDate}>{formatDate(date)}</div>
      </div>
      <Button>Atender</Button>
    </div>
  );
};

export default ListItem;
