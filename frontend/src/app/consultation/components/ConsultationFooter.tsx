import { MouseEventHandler } from 'react';
import styles from '../styles/ConsultationFooter.module.css';
import Button from "support/components/Button";

type Props = {
  handleHelp: MouseEventHandler;
  handleCreate: MouseEventHandler;
}

const ConsultationFooter = ({handleHelp, handleCreate}: Props) => {
  return (
    <div className={styles.footer}>
      <Button outlined onClick={handleHelp}>Ajuda</Button>
      <Button onClick={handleCreate}>Agendar consulta</Button>
    </div>
  );
}

export default ConsultationFooter;