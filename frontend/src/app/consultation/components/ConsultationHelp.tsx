import React from "react";

const ConsultationHelp: React.FC = () => {
  return (
    <div>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec
        urna et enim placerat cursus ac nec orci. Vivamus imperdiet hendrerit
        tincidunt. Fusce dui dui, laoreet quis augue ut, congue varius tellus.
        Sed eget consectetur eros. Cras a congue odio. Mauris convallis nunc id
        risus pellentesque, ut tristique ipsum sagittis. Proin consequat metus
        tincidunt, lacinia ex ac, volutpat lectus. Sed dignissim augue tellus,
        eu condimentum leo dapibus et. Cras bibendum viverra nulla cursus
        dictum. Orci varius natoque penatibus et magnis dis parturient montes,
        nascetur ridiculus mus.
      </p>
      <p>
        Duis sollicitudin, sapien id mollis semper, magna massa condimentum
        eros, eget egestas risus orci molestie metus. Sed consectetur ex id enim
        mattis eleifend. Maecenas dictum feugiat volutpat. Sed porttitor
        condimentum nisl eu luctus. Pellentesque nec volutpat mi, sit amet
        pulvinar diam. Ut fermentum ipsum nunc, et pretium erat faucibus sed.
        Duis gravida lacinia nisl eget feugiat. Praesent tincidunt mi id lacinia
        feugiat. Nulla metus metus, sagittis at sem sit amet, consequat gravida
        lorem. Donec ac commodo tellus, at euismod nisi. Fusce in posuere justo.
        Integer id tincidunt tortor.
      </p>
    </div>
  );
};

export default ConsultationHelp;
