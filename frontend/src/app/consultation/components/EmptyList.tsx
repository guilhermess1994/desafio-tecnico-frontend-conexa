import React from 'react';
import Papers from "assets/papers.svg";
import Plant from "assets/plant.svg";
import styles from '../styles/ConsultationList.module.css';

const EmptyList: React.FC = () => {
  return (
    <div className={styles.emptyListWrapper}>
      <div className={styles.emptyRight }>
        <img src={Papers} alt="papers"/>
      </div>
      <div className={styles.emptyCenter }>
        Não há nenhuma consulta agendada
      </div>
      <div className={styles.emptyLeft }>
        <img src={Plant} alt="plant" />
      </div>
    </div>
  );
}

export default EmptyList;