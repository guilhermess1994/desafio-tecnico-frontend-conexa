import { render } from '@testing-library/react';
import App from '../App';

test('app should have childrens', () => {
  render(<App />);

  const header = document.getElementsByClassName('header');
  expect(header.length).toBeTruthy();

  const container = document.getElementsByClassName('container');
  expect(container.length).toBeTruthy();
});
