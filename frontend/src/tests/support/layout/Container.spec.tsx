import { render, screen } from '@testing-library/react';
import Container from 'support/layout/Container';

test('when is not authenticated should not render username', () => {
  render(<Container>child</Container>);

  const linkElement = screen.getByText(/child/i);
  expect(linkElement).toBeInTheDocument();
});