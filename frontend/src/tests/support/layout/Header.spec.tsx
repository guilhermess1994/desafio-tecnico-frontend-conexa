import { render } from '@testing-library/react';
import Header from 'support/layout/Header';

test('when is not authenticated should not render username', () => {
  render(<Header isAuthenticated={false}/>);

  const username = document.getElementsByClassName('username');
  expect(username.length).toBeFalsy();
});

test('when is authenticated should render username', () => {
  render(<Header isAuthenticated={true}/>);

  const username = document.getElementsByClassName('username');
  expect(username.length).toBeTruthy();
});
